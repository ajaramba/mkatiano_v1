<?php

/**
 * Main entry point for the app
 *
 * @author iddsalim
 */

 class Main extends MX_Controller{
    //put your code here
    
    public function main (){
        parent::__construct();
    }
    
    public function index() {

        //Check if logged on and send to dashboard
        if ( ! ($this->session->userdata('pkid') == "") ) {
            $data['page_title'] = "Mkatiano 1.0.1 - Dashboard";
            $data['page_section'] = "timeline";
            $data['username'] = $this->session->userdata('name');
            $data['lastlogin'] = $this->session->userdata('lastlogin');
            $data['logincount'] = $this->session->userdata('logincount');
            $this->load->view("mkatiano_dashboard", $data);
        } 
        else //Else, send to login page
        {    
            redirect(base_url()."login", 'location');
        }
    
    }
    
    public function show_passwordreset() {
        $data['page_title'] = "Mkatiano 1.0.1 - Login - PHP/JQuery/PostGres on wheels";
        $this->load->view("mkatiano_lostpassword", $data);                
    }
    
    public function show_login() {
        $data['page_title'] = "Mkatiano 1.0.1 - Login - PHP/JQuery/PostGres on wheels";
        $this->load->view("mkatiano_login", $data);                
    }
    
    public function do_logout() {
        $this->session->sess_destroy();
        redirect("/",'location');
    }
    
    public function show_join() {
        $data['page_title'] = "Mkatiano 1.0.1 - Join - PHP/JQuery/PostGres on wheels";
        $this->load->view("mkatiano_join", $data);                
    }
    
    public function do_login() {
        if ($_POST['mk_uemail'] ==="" ||  $_POST['mk_pass'] ==="") {
            $resp = '{"status":"0","txt":"Both username and password must be supplied."}';
        } else {
            $resp = mkatiano_DB_userLogin($_POST['mk_uemail'], $_POST['mk_pass']);
        }
        echo $resp;
    }
    
    public function do_join() {
        if ($_POST['mk_uemail'] ==="" ||  $_POST['mk_ufullnames'] ==="" ||  $_POST['mk_gender'] ==="") {
            $resp = '{"status":"0","txt":"Kindly fill in ALL the fields provided."}';
        } else {
            $resp = mkatiano_DB_userAdd($_POST['mk_uemail'], $_POST['mk_ufullnames'],  $_POST['mk_gender']);
        }
        echo $resp;
    }
    
    public function do_changepass() {
        if ($_POST['cPass'] ==="" ||  $_POST['nPass'] ==="" ||  $_POST['nPassC'] ==="") {
            $resp = '{"status":"0","txt":"Kindly fill in ALL the fields provided."}';
        } elseif ($_POST['nPass'] !== $_POST['nPassC']) {
            $resp = '{"status":"0","txt":"The NEW password and the confirmation MUST match."}';
        } else {
            $resp = mkatiano_DB_passwordChange($this->session->userdata('pkid'), $_POST['cPass'], $_POST['nPass'],  $this->session->userdata('name'));
        }
        echo $resp;
    }
    
    public function do_passwordreset() {
        if ($_POST['mk_uemail'] ==="") {
            $resp = '{"status":"0","txt":"Kindly fill in ALL the fields provided."}';
        } else {
            // There are two options when it comes to password changes : 
            // 1 -Change password and send new password to user
            // 2 -Send user an email with a link that takes them to a change-password form
            // **We choose option 1 in this version.
            
            $resp = mkatiano_DB_userResetPass($_POST['mk_uemail'], $this->session->userdata('name'));
        }
        echo $resp;
    }

    public function section($section="timeline") {

        //Check if logged on and send to section
        if ( ! ($this->session->userdata('pkid') == "") ) {
            $data['page_title'] = "Mkatiano 1.0.1 - ".$section ;
            $data['page_section'] = $section;
            $data['username'] = $this->session->userdata('name');
            $data['lastlogin'] = $this->session->userdata('lastlogin');
            $data['logincount'] = $this->session->userdata('logincount');
            $this->load->view("mkatiano_dashboard", $data);
        } 
        else //Else, send to login page
        {    
            redirect(base_url()."login", 'location');
        }
    
    }
    
    
}

?>
