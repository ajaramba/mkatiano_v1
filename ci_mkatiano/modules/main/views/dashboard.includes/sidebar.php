<div id="sidebar">
        <div class="box">
            <div class="h_title">&#8250; Main control</div>
            <ul id="home">
                <li class="b1"><a class="icon view_page" href="/">My Timeline</a></li>
                <li class="b2"><a class="icon report" href="/profile">My Profile</a></li>
                <li class="b1"><a class="icon add_page" href="/friends">My Friends</a></li>
            </ul>
        </div>

        <div class="box">
                <div class="h_title">&#8250; Chat</div>
                <ul>
                    <li class="b2"><a class="icon contact" href="">No friends online :(</a></li>
                </ul>
        </div>
    
        <div class="box">
                <div class="h_title">&#8250; Settings</div>
                <ul>
                    <li class="b2"><a class="icon contact" href="/passwordchange">Change Password</a></li>
                    <li class="b1"><a class="icon config" href="/preferences">Account preferences</a></li>
                </ul>
        </div>
</div>