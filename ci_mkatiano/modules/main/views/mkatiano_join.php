<!-- Libraries -->
    <? include 'ci_mkatiano/helpers/mk_header.php'; ?>
    <? include 'ci_mkatiano/helpers/login_js_css.php'; ?>
<!-- End of Libraries -->

</head>
    <body>
    <div class="wrap">
        <div id="content">
            <div id="main">
                <div class="full_w">
                    <p class="descr">Join Mkatiano. <br/> Fill in the form below and click on 'Join'</p>
                    
                    <form action="" method="post" id="joinForm">
                        <label for="mk_uemail">Email Address:</label>
                        <input id="mk_uemail" name="mk_uemail" class="text" />
                        
                        <label for="mk_ufullnames">Full Names:</label>
                        <input id="mk_ufullnames" name="mk_ufullnames" class="text" />
                        
                        <label for="mk_gender">Gender :</label>
                        <select id="mk_gender" name="mk_gender" class="text">
                            <option value="">-Select-</option>
                            <option value="F">Female</option>
                            <option value="M">Male</option>
                        </select>
                        
                        <div class="sep"></div>
                        
                        <center>
                            <button type="submit" class="add">Create account</button> 
                            <br/> <br/>
                            Already a member?<br/>
                            <a class="button ok" href="/login">Login</a>
                        </center>
                            
                    </form>
                </div>
                <div class="footer">&raquo; <a href="http://www.iddsalim.com/">By Idd Salim</a> </div>
            </div>
        </div>
    </div>
    </body>
</html>
