<?php

    $lang['huduma_global_systemname'] = "Huduma";
    $lang['huduma_global_systemdomain'] = "dev.huduma.or.ke";
    $lang['huduma_global_title'] = "Huduma - Fix my constituency!";

    $lang['huduma_global_homepage_welcome'] = "Welcome";
    $lang['huduma_global_homepage_selectlanguage'] = "Select language :";
    $lang['huduma_global_homepage_about'] = "The Huduma platform was built for information collection and interactive mapping";

    $lang['huduma_global_homepage_mapquickfilter'] = "Quick filter : ";
    $lang['huduma_global_homepage_maptoday'] = "Today";
    $lang['huduma_global_homepage_map7days'] = "Past 1 week";
    $lang['huduma_global_homepage_map30days'] = "Past 1 month";
    $lang['huduma_global_homepage_map90days'] = "Past 3 months";
    $lang['huduma_global_homepage_map180days'] = "Past 6 months";
    $lang['huduma_global_homepage_map365days'] = "Past 1 year";

    $lang['huduma_global_homepage_maphowtoreport'] = "How to report";
    $lang['huduma_global_homepage_maphowtoreportsms'] = "SMS";
    $lang['huduma_global_homepage_maphowtoreportemail'] = "Email";
    $lang['huduma_global_homepage_maphowtoreporttwitter'] = "Twitter";

    $lang['huduma_global_homepage_popularreports'] = "Most popular reports";

    $lang['huduma_global_links_home'] = "Home";
    $lang['huduma_global_links_reportissue'] = "Report an issue";
    $lang['huduma_global_links_alerts'] = "Get alerts";
    $lang['huduma_global_links_contactus'] = "Contact us";

    $lang['huduma_global_loginerror_fail'] = "Invalid login details. Please try again";
    $lang['huduma_global_loginerror_inactive'] = "Login was successful but your account has not been activated yet. <br/>Please await activation email";

    $lang['huduma_global_sms_alerts'] = "Thanks you ~#PERSONNAME~ for subscribing to ~#SYSTEMNAME~ Alerts. Your confirmation code is : \n~#CONFIRMCODE~\n. Go to ~#SYSTEMDOMAIN~ and enter this code under Alerts Menu";
    $lang['huduma_global_sms_contactus'] = "How to report";
    $lang['huduma_global_sms_join'] = "How to report";
    $lang['huduma_global_sms_smsreport'] = "How to report";

    $lang['huduma_global_email_subject_alerts'] = "Huduma Service - Alert Confirmation";

?>
