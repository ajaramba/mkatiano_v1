--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: mk_emails; Type: TABLE; Schema: public; Owner: mdosi; Tablespace: 
--

CREATE TABLE mk_emails (
    pkid integer NOT NULL,
    e_recepient character varying(64) NOT NULL,
    e_message character varying(512),
    e_queuetime timestamp without time zone DEFAULT '2012-09-11 07:44:42.503202'::timestamp without time zone NOT NULL,
    e_sent integer DEFAULT 0 NOT NULL,
    e_sendtime timestamp without time zone,
    e_subject character varying(64)
);


ALTER TABLE public.mk_emails OWNER TO mdosi;

--
-- Name: COLUMN mk_emails.pkid; Type: COMMENT; Schema: public; Owner: mdosi
--

COMMENT ON COLUMN mk_emails.pkid IS 'Primary Key of the email log';


--
-- Name: COLUMN mk_emails.e_recepient; Type: COMMENT; Schema: public; Owner: mdosi
--

COMMENT ON COLUMN mk_emails.e_recepient IS 'Email recepient';


--
-- Name: COLUMN mk_emails.e_message; Type: COMMENT; Schema: public; Owner: mdosi
--

COMMENT ON COLUMN mk_emails.e_message IS 'Email message';


--
-- Name: mk_emails_pkid_seq; Type: SEQUENCE; Schema: public; Owner: mdosi
--

CREATE SEQUENCE mk_emails_pkid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mk_emails_pkid_seq OWNER TO mdosi;

--
-- Name: mk_emails_pkid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mdosi
--

ALTER SEQUENCE mk_emails_pkid_seq OWNED BY mk_emails.pkid;


--
-- Name: mk_emails_pkid_seq; Type: SEQUENCE SET; Schema: public; Owner: mdosi
--

SELECT pg_catalog.setval('mk_emails_pkid_seq', 24, true);


--
-- Name: mk_userpreferences; Type: TABLE; Schema: public; Owner: mdosi; Tablespace: 
--

CREATE TABLE mk_userpreferences (
    pkid integer NOT NULL,
    user_id integer,
    up_emailonlike smallint,
    up_smsonlike smallint
);


ALTER TABLE public.mk_userpreferences OWNER TO mdosi;

--
-- Name: TABLE mk_userpreferences; Type: COMMENT; Schema: public; Owner: mdosi
--

COMMENT ON TABLE mk_userpreferences IS 'able for users to save preferences';


--
-- Name: COLUMN mk_userpreferences.pkid; Type: COMMENT; Schema: public; Owner: mdosi
--

COMMENT ON COLUMN mk_userpreferences.pkid IS 'running primary key';


--
-- Name: mk_userpreferences_pkid_seq; Type: SEQUENCE; Schema: public; Owner: mdosi
--

CREATE SEQUENCE mk_userpreferences_pkid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mk_userpreferences_pkid_seq OWNER TO mdosi;

--
-- Name: mk_userpreferences_pkid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mdosi
--

ALTER SEQUENCE mk_userpreferences_pkid_seq OWNED BY mk_userpreferences.pkid;


--
-- Name: mk_userpreferences_pkid_seq; Type: SEQUENCE SET; Schema: public; Owner: mdosi
--

SELECT pg_catalog.setval('mk_userpreferences_pkid_seq', 2, true);


--
-- Name: mk_users; Type: TABLE; Schema: public; Owner: mdosi; Tablespace: 
--

CREATE TABLE mk_users (
    pkid integer NOT NULL,
    u_email character varying(64),
    u_password character varying(128),
    u_fullnames character varying(64),
    u_joindate timestamp without time zone DEFAULT '2012-09-10 17:24:48.684162'::timestamp without time zone NOT NULL,
    u_lastlogin timestamp without time zone,
    u_logincount integer DEFAULT 0 NOT NULL,
    u_active smallint DEFAULT 1 NOT NULL,
    u_gender "char" NOT NULL
);


ALTER TABLE public.mk_users OWNER TO mdosi;

--
-- Name: TABLE mk_users; Type: COMMENT; Schema: public; Owner: mdosi
--

COMMENT ON TABLE mk_users IS 'Users table.

Stores fullnames, email, password, join_date, last_login, login_count

';


--
-- Name: COLUMN mk_users.pkid; Type: COMMENT; Schema: public; Owner: mdosi
--

COMMENT ON COLUMN mk_users.pkid IS 'Primary key - AutoIncrement';


--
-- Name: COLUMN mk_users.u_email; Type: COMMENT; Schema: public; Owner: mdosi
--

COMMENT ON COLUMN mk_users.u_email IS 'email address';


--
-- Name: mk_users_pkid_seq; Type: SEQUENCE; Schema: public; Owner: mdosi
--

CREATE SEQUENCE mk_users_pkid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mk_users_pkid_seq OWNER TO mdosi;

--
-- Name: mk_users_pkid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mdosi
--

ALTER SEQUENCE mk_users_pkid_seq OWNED BY mk_users.pkid;


--
-- Name: mk_users_pkid_seq; Type: SEQUENCE SET; Schema: public; Owner: mdosi
--

SELECT pg_catalog.setval('mk_users_pkid_seq', 14, true);


--
-- Name: pkid; Type: DEFAULT; Schema: public; Owner: mdosi
--

ALTER TABLE ONLY mk_emails ALTER COLUMN pkid SET DEFAULT nextval('mk_emails_pkid_seq'::regclass);


--
-- Name: pkid; Type: DEFAULT; Schema: public; Owner: mdosi
--

ALTER TABLE ONLY mk_userpreferences ALTER COLUMN pkid SET DEFAULT nextval('mk_userpreferences_pkid_seq'::regclass);


--
-- Name: pkid; Type: DEFAULT; Schema: public; Owner: mdosi
--

ALTER TABLE ONLY mk_users ALTER COLUMN pkid SET DEFAULT nextval('mk_users_pkid_seq'::regclass);


--
-- Data for Name: mk_emails; Type: TABLE DATA; Schema: public; Owner: mdosi
--

COPY mk_emails (pkid, e_recepient, e_message, e_queuetime, e_sent, e_sendtime, e_subject) FROM stdin;
23	iddsalim@xema.mobi	Hello Idi Sarim<br/><br/>Welcome to Mkatiano. Your temporary password is : JN94wNZq<br/><br/>Please login and change this to a password you are more comfortable with.<br/><br/>Jibambe mzito.<br/><br/>	2012-09-11 07:44:42.503202	1	2012-09-24 12:00:37	Welcome to Mkatiano
9	iddsalim@gmail.com	Hello Kis Mode<br/><br/>Welcome to Mkatiano. Your temporary password is : lYsWTUq5<br/><br/>Jibambe mzito.<br/><br/>	2012-09-11 07:44:42.503202	1	\N	\N
10	coder@xema.mobi	Hello Idd Xalim<br/><br/>Welcome to Mkatiano. Your temporary password is : Fago7pWv<br/><br/>Jibambe mzito.<br/><br/>	2012-09-11 07:44:42.503202	1	\N	\N
11	iddsalim@xema.mobi	Hello Kis Modddddde<br/><br/>Welcome to Mkatiano. Your temporary password is : N1noc2yP<br/><br/>Jibambe mzito.<br/><br/>	2012-09-11 07:44:42.503202	1	\N	\N
12	iddsalim@gmail.com	Hello <br/><br/>Your Mkatiano password has been reset. Your temporary password is : R9ZvXK8o.<br/><br/>Please login and change this to a password you are more comfortable with.<br/><br/>Jibambe mzito.<br/><br/>	2012-09-11 07:44:42.503202	1	\N	\N
13	iddsalim@gmail.com	Hello <br/><br/>Your Mkatiano password has been reset. Your temporary password is : Ur76WjJO.<br/><br/>Please login and change this to a password you are more comfortable with.<br/><br/>Jibambe mzito.<br/><br/>	2012-09-11 07:44:42.503202	1	\N	\N
14	iddsalim@gmail.com	Hello <br/><br/>Your Mkatiano password has been reset. Your temporary password is : IqfCekMp.<br/><br/>Please login and change this to a password you are more comfortable with.<br/><br/>Jibambe mzito.<br/><br/>	2012-09-11 07:44:42.503202	1	\N	\N
15	coder@xema.mobi	Hello CO Da<br/><br/>Welcome to Mkatiano. Your temporary password is : BOdDYUjy<br/><br/>Please login and change this to a password you are more comfortable with.<br/><br/>Jibambe mzito.<br/><br/>	2012-09-11 07:44:42.503202	1	\N	\N
17	iddsalim@gmail.com	Hello <br/><br/>Your Mkatiano password has been reset. Your temporary password is : uFI7svCW.<br/><br/>Please login and change this to a password you are more comfortable with.<br/><br/>Jibambe mzito.<br/><br/>	2012-09-11 07:44:42.503202	1	\N	\N
18	iddsalim@gmail.com	Hello Kis Mode<br/><br/>Your Mkatiano password has been changed to the one you specified.<br/><br/>Kindly use the new password you specified for future logins.<br/><br/>Jibambe mzito.<br/><br/>	2012-09-11 07:44:42.503202	1	\N	\N
19	iddsalim@gmail.com	Hello <br/><br/>Your Mkatiano password has been reset. Your temporary password is : pD8SKG1E.<br/><br/>Please login and change this to a password you are more comfortable with.<br/><br/>Jibambe mzito.<br/><br/>	2012-09-11 07:44:42.503202	1	2012-09-24 11:50:56	Mkatiano password reset
20	iddsalim@gmail.com	Hello Kis Mode<br/><br/>Your Mkatiano password has been changed to the one you specified.<br/><br/>Kindly use the new password you specified for future logins.<br/><br/>Jibambe mzito.<br/><br/>	2012-09-11 07:44:42.503202	1	2012-09-24 11:55:01	Mkatiano password changed
21	iddsalim@gmail.com	Hello <br/><br/>Your Mkatiano password has been reset. Your temporary password is : Y2a3Tkeq.<br/><br/>Please login and change this to a password you are more comfortable with.<br/><br/>Jibambe mzito.<br/><br/>	2012-09-11 07:44:42.503202	1	2012-09-24 11:55:11	Mkatiano password reset
22	iddsalim@gmail.com	Hello Kis Mode<br/><br/>Your Mkatiano password has been changed to the one you specified.<br/><br/>Kindly use the new password you specified for future logins.<br/><br/>Jibambe mzito.<br/><br/>	2012-09-11 07:44:42.503202	1	2012-09-24 12:00:29	Mkatiano password changed
24	iddsalim@xema.mobi	Hello Idi Sarim<br/><br/>Your Mkatiano password has been changed to the one you specified.<br/><br/>Kindly use the new password you specified for future logins.<br/><br/>Jibambe mzito.<br/><br/>	2012-09-11 07:44:42.503202	1	2012-09-24 12:27:03	Mkatiano password changed
\.


--
-- Data for Name: mk_userpreferences; Type: TABLE DATA; Schema: public; Owner: mdosi
--

COPY mk_userpreferences (pkid, user_id, up_emailonlike, up_smsonlike) FROM stdin;
1	13	1	1
2	14	1	1
\.


--
-- Data for Name: mk_users; Type: TABLE DATA; Schema: public; Owner: mdosi
--

COPY mk_users (pkid, u_email, u_password, u_fullnames, u_joindate, u_lastlogin, u_logincount, u_active, u_gender) FROM stdin;
13	coder@xema.mobi	bdgwHnzDRJiaDFGMJfVP+nid6/V0ayYfUtX/bRehX6U=	CO Da	2012-09-10 17:24:48.684162	\N	0	0	F
14	iddsalim@xema.mobi	yogulDliLfezKfgjpZs5eUTOSz4qArrMkzwsHwjAofE=	Idi Sarim	2012-09-10 17:24:48.684162	2012-09-24 12:09:04	2	1	M
10	iddsalim@gmail.com	yogulDliLfezKfgjpZs5eUTOSz4qArrMkzwsHwjAofE=	Kis Mode	2012-09-10 17:24:48.684162	2012-09-24 12:51:18	22	1	F
\.


--
-- Name: const_emails; Type: CONSTRAINT; Schema: public; Owner: mdosi; Tablespace: 
--

ALTER TABLE ONLY mk_users
    ADD CONSTRAINT const_emails UNIQUE (u_email);


--
-- Name: pk; Type: CONSTRAINT; Schema: public; Owner: mdosi; Tablespace: 
--

ALTER TABLE ONLY mk_userpreferences
    ADD CONSTRAINT pk PRIMARY KEY (pkid);


--
-- Name: pk_emails; Type: CONSTRAINT; Schema: public; Owner: mdosi; Tablespace: 
--

ALTER TABLE ONLY mk_emails
    ADD CONSTRAINT pk_emails PRIMARY KEY (pkid);


--
-- Name: pk_users; Type: CONSTRAINT; Schema: public; Owner: mdosi; Tablespace: 
--

ALTER TABLE ONLY mk_users
    ADD CONSTRAINT pk_users PRIMARY KEY (pkid);


--
-- Name: up_userid; Type: FK CONSTRAINT; Schema: public; Owner: mdosi
--

ALTER TABLE ONLY mk_userpreferences
    ADD CONSTRAINT up_userid FOREIGN KEY (user_id) REFERENCES mk_users(pkid) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

