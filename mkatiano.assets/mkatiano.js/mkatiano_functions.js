// Jquery with no conflict
jQuery(document).ready(function($) {

    $(".box .h_title").not(this).next("ul").hide("normal");
    $(".box .h_title").not(this).next("#home").show("normal");
    $(".box").children(".h_title").click( function() { $(this).next("ul").slideToggle(); });

    $('form#uploadContent').submit(function (e) {
        mkatiano_uploadAndPost_ajax( "#submit", "Upload article", "/do_upload", 'cContentFile', 0 );
        e.preventDefault();
    });
        
    $('form#resetPassForm').submit(function (e) {
        mkatiano_performAjaxPost('#submit', "Reset password", "Resetting...",'resetpassword', '#resetPassForm', "/", 0);
        e.preventDefault();
    });
    
    $('form#joinForm').submit(function (e) {
        mkatiano_performAjaxPost('#submit', "Create account", "Creating account...",'createaccount', '#joinForm', "/", 0);
        e.preventDefault();
    });
    
    $('form#loginForm').submit(function (e) {
        mkatiano_performAjaxPost('#submit', "Login", "Wait...",'loginuser', '#loginForm', "/", 0);
        e.preventDefault();
    });
    
    $('form#passChangeForm').submit(function (e) {
        mkatiano_performAjaxPost('#submit', "Change password", "Wait...",'changepass', '#passChangeForm', "/", 0);
        e.preventDefault();
    });
    
    function mkatiano_performAjaxPost(commandButton, initialText, progressText, postURL, formName, finalDestination, isPopup){
        $(commandButton).val(progressText).attr("disabled","disabled");
        $.ajax({
            type: "POST",
            url: "/"+postURL,
            data: $(formName).serialize(),
            dataType: "json",
            success: function(msg){
                if(parseInt(msg.status)==1)
                {
                    if (isPopup==1) {
                        alert('Operation was completed successfully');
                        parent.$.fn.colorbox.close(); 
                    } else {
                        if (msg.txt.toString() !=="") {alert (msg.txt);}
                        window.location =finalDestination;
                    }
                    return true;
                }
                else if(parseInt(msg.status)==0)
                {
                    $(commandButton).removeAttr("disabled").val(initialText);
                    alert (msg.txt);
                }
            },
            error: function(msg){
                alert ('You seem offline. Could not contact Mkatiano service. Try again.');
                $(commandButton).removeAttr("disabled").val(initialText);
            }
        });
    }

    function mkatiano_uploadAndPost_ajax(commandButton, uploadProgressText, uploadURL, uploadField, isLogo){
        $(commandButton).val(uploadProgressText).attr("disabled","disabled");

            $.ajaxFileUpload(
            {
                url             : uploadURL+"/"+isLogo,
                secureuri       : false,
                fileElementId   : uploadField,
                dataType        : 'json',
                success: function (msg)
                {
                    if(parseInt(msg.status)==1)
                    {
                        $("input#upl_name").val(msg.att_name);
                        $("input#upl_mime").val(msg.att_ext);
                        //mkatiano_registerContent_ajax();
                        if (isLogo==1) {
                            mkatiano_performAjaxPost('#submit', "Update profile information", "Updating details...",'updateprofile', '#uploadProfileForm', "/profile/");
                        } else {
                            mkatiano_performAjaxPost('#submit', "Upload article", "Uploading content...",'registercontent', '#uploadContent', "/content/");
                        }
                    }
                    else if(parseInt(msg.status)==0)
                    {
                        if (isLogo==1) {
                            $('#submit').removeAttr("disabled").val("Update profile information");
                        } else {
                            $('#submit').removeAttr("disabled").val("Upload article");                                                    
                        }
                        alert (msg.txt);
                    }
                },
                error: function (data, status, e)
                    {
                        alert(e);
                        if (isLogo==1) {
                            $('#submit').removeAttr("disabled").val("Update profile information");                        
                        } else {
                            $('#submit').removeAttr("disabled").val("Upload article");                            
                        }
                    }
                }
            );
    }


//close			
});
	

