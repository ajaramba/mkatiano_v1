/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

// Jquery with no conflict
jQuery(document).ready(function($) {

    // Poshytips ------------------------------------------------------ //
    $('.poshytip').poshytip({
    	className: 'tip-darkgray',
        showTimeout: 1,
        alignTo: 'target',
        alignX: 'center',
        offsetY: 5,
        allowTipHover: false
    });
  
    // Poshytips Forms ------------------------------------------------------ //
    $('.form-poshytip').poshytip({
        className: 'tip-darkgray',
        showOn: 'focus',
        alignTo: 'target',
        alignX: 'right',
        alignY: 'center',
        offsetX: 5
    });

    hideLoader();
    
    if ($("div#secoUniWing") != 'undefined') {
        $("div#secoUniWing").hide();
        $("div#secoUniYear").hide();
        $("div#secoUniTime").hide();
    }
    
    //show loading bar
    function showLoader(){
        $('.search-background').fadeIn(200);
    }

    //hide loading bar  
    function hideLoader(){
        $('#sub_cont').fadeIn(1500);
        $('.search-background').fadeOut(200);
    };
    
    $("select#cDiscipline").change(function(){
        $.getJSON("/getjson/c/"+$(this).val(), function(j){
            var options = '<option value="0">- Select -</option>';
                for (var i = 0; i < j.length; i++) {
                    options += '<option value="' + j[i].optionValue + '">' + j[i].optionDisplay + '</option>';
                }
            $("select#cCategories").html(options);
        });       
    });

    $("select#cCategories").change(function(){
        $.getJSON("/getjson/s/"+$(this).val(), function(j){
            var options = '<option value="0">- Select -</option>';
                for (var i = 0; i < j.length; i++) {
                    options += '<option value="' + j[i].optionValue + '">' + j[i].optionDisplay + '</option>';
                }
            $("select#cSubject").html(options);
        });       
    });

    $("select#cSubject").change(function(){
        $('#subjID').val($('#cSubject option:selected').val());
    });

    $("#cField").tokenInput("/getjson/f/", {
        theme: "facebook",
        tokenLimit: 1
    });
    
    
    $("select#cInstitution").change(function(){
        
        if ($("select#cInstitution").val() == "High school" || $("select#cInstitution").val() =="University – Undergraduate" ) {
            $("div#secoUniWing").css("visibility","visible").fadeIn(100);
            $("div#secoUniYear").css("visibility","visible").fadeIn(100);
            $("div#secoUniTime").css("visibility","visible").fadeIn(100);
        } else {
            $("div#secoUniWing").css("visibility","hidden").fadeOut(100);
            $("div#secoUniYear").css("visibility","hidden").fadeOut(100);
            $("div#secoUniTime").css("visibility","hidden").fadeOut(100);
        }
        e.preventDefault();
    });

    $(".popupBookShelves").colorbox({iframe:true, innerWidth:450, innerHeight:300});
    $(".popupAuthors").colorbox({
        iframe:true, innerWidth:550, innerHeight:300,
        onClosed:function(){window.location ="/content/";}        
    });

    $('#cmdAdd').click(function(e) {
        var selectedOpts = $('#lstAllAuthors option:selected');
        if (selectedOpts.length == 0) {
            alert("Please select at least one author.");
            e.preventDefault();
        }

        $('#lstCurrAuthors').append($(selectedOpts).clone());
        $(selectedOpts).remove();
        e.preventDefault();
    });

    $('#cmdRemove').click(function(e) {
        var selectedOpts = $('#lstCurrAuthors option:selected');
        if (selectedOpts.length == 0) {
            alert("Please select an author to remove .");
            e.preventDefault();
        }

        $('#lstAllAuthors').append($(selectedOpts).clone());
        $(selectedOpts).remove();
        e.preventDefault();
    });

    $('form#uploadContent').submit(function (e) {
        //hadithi_uploadContent_ajax();
        hadithi_uploadAndPost_ajax( "#submit", "Upload article", "/do_upload", 'cContentFile', 0 );
        e.preventDefault();
    });
    
    $('form#searchContent').submit(function (e) {
        hadithi_searchContent_ajax();
        e.preventDefault();
    });
    
    $('form#registerForm').submit(function (e) {
        //hadithi_registerAccount_ajax();
        hadithi_performAjaxPost('#submit', "Create account", "Creating account...",'createaccount', '#registerForm', "/", 0);
        e.preventDefault();
    });
    
    $('form#loginForm').submit(function (e) {
        hadithi_performAjaxPost('#submit', "Login to hadithi", "Authenticating...",'authenticate', '#loginForm', "/", 0);
        e.preventDefault();
    });
    
    $('form#addBookShelfForm').submit(function (e) {
        hadithi_performAjaxPost('#submit', "Save bookshelf", "Saving bookshelf...",'savebookshelf', '#addBookShelfForm', "/bookshelves/", 0);
        e.preventDefault();
    });
    
    $('form#addBookShelfPopupForm').submit(function (e) {
        var cN = $("#txtCName").val();
        var cID = $("#txtCID").val();        
        hadithi_performAjaxPost('#submit', "Save bookshelf", "Saving bookshelf...",'savebookshelf', '#addBookShelfPopupForm', "/popupbookshelves/"+cID+"/"+cN, 0);
        e.preventDefault();
    });
    
    $('form#addAuthorForm').submit(function (e) {
        hadithi_performAjaxPost('#submit', "Save author", "Saving author...",'saveauthor', '#addAuthorForm', "/authors/", 0);
        e.preventDefault();
    });
    
    $('form#changePasswordForm').submit(function (e) {
        hadithi_performAjaxPost('#submit', "Change your password", "Changing password...",'updatepassword', '#changePasswordForm', "/profile/", 0);
        e.preventDefault();
    });
    
    $('form#addtoBookShelfForm').submit(function (e) {
        var artNum = $("#artID").val();
        hadithi_performAjaxPost('#submit', "Add", "Adding...",'addtobookshelf', '#addtoBookShelfForm', "/article/"+artNum, 1);
        e.preventDefault();
    });
    
    $('form#addContentAuthorForm').submit(function (e) {
        //var artNum = $("#artID").val();
        
        var currAuthors= "";
        $("#lstCurrAuthors option").each(function(){ 
            currAuthors += "___-_"+ $(this).val(); 
        });
        
        $("#authorsID").val(currAuthors);
        
        hadithi_performAjaxPost('#submit', "Save", "Saving...",'updatecontentauthors', '#addContentAuthorForm', "/content/", 1);
        e.preventDefault();
    });
    
    $('form#uploadProfileForm').submit(function (e) {
        //hadithi_performAjaxPost('#submit', "Change your password", "Changing password...",'updatepassword', '#changePasswordForm', "/profile/", 0);
        hadithi_uploadAndPost_ajax('#submit', "Uploading logo...", "/do_upload", 'cContentFile', 1);
        e.preventDefault();
    });
    
    $('a#applyFilter').click(function (e) {
        $('form#refineSearchForm').submit();
    });
    
    $('form#refineSearchForm').submit(function (e) {
        hadithi_performAjaxSearch('refinesearch', '#refineSearchForm');
        e.preventDefault();
    });
    
    $('a#activateThis').click(function (e) {
        var cID = $(this).attr("itemID");
        var cFunct = $(this).attr("cFunct"); 
        var cReturn = $(this).attr("cReturn"); 
        hadithi_performAjaxOperation(cFunct, "/"+ cReturn +"/", 1, cID);
        e.preventDefault();
    });
    
    $('a#deactivateThis').click(function (e) {
        var cID = $(this).attr("itemID");
        var cFunct = $(this).attr("cFunct"); 
        var cReturn = $(this).attr("cReturn"); 
        hadithi_performAjaxOperation(cFunct, "/"+ cReturn +"/", 0, cID);
        e.preventDefault();
    });
    
    $('a#deleteThis').click(function (e) {
        var cID = $(this).attr("itemID");
        var cFunct = $(this).attr("cFunct"); 
        var cReturn = $(this).attr("cReturn"); 
        hadithi_performAjaxOperation(cFunct, "/"+ cReturn +"/", 2, cID);
        e.preventDefault();
    });

    function hadithi_performAjaxSearch(postURL, formName){
        $("div#divResults").html("<center><img src='/hadithi.assets/hadithi.images/loading.gif'> Filtering results...</center>");
        $.ajax({
            type: "POST",
            url: "/"+postURL,
            data: $(formName).serialize(),
            dataType: "html",
            success: function(msg){
                //window.location =postURL;
                $("div#divResults").html(msg);
                return true;
            },
            error: function(msg){
                alert ('You seem offline. Could not contact Hadithi Service. Try again.');
            }
        });
    }

    function hadithi_performAjaxPost(commandButton, initialText, progressText, postURL, formName, finalDestination, isPopup){
        $(commandButton).val(progressText).attr("disabled","disabled");
        $.ajax({
            type: "POST",
            url: "/"+postURL,
            data: $(formName).serialize(),
            dataType: "json",
            success: function(msg){
                if(parseInt(msg.status)==1)
                {
                    if (isPopup==1) {
                        alert('Operation was completed successfully');
                        parent.$.fn.colorbox.close(); 
                    } else {
                        window.location =finalDestination;
                    }
                    return true;
                }
                else if(parseInt(msg.status)==0)
                {
                    $(commandButton).removeAttr("disabled").val(initialText);
                    alert (msg.txt);
                }
            },
            error: function(msg){
                alert ('You seem offline. Could not contact Hadithi Service. Try again.');
                $(commandButton).removeAttr("disabled").val(initialText);
            }
        });
    }

    function hadithi_uploadAndPost_ajax(commandButton, uploadProgressText, uploadURL, uploadField, isLogo){
        $(commandButton).val(uploadProgressText).attr("disabled","disabled");

            $.ajaxFileUpload(
            {
                url             : uploadURL+"/"+isLogo,
                secureuri       : false,
                fileElementId   : uploadField,
                dataType        : 'json',
                success: function (msg)
                {
                    if(parseInt(msg.status)==1)
                    {
                        $("input#upl_name").val(msg.att_name);
                        $("input#upl_mime").val(msg.att_ext);
                        //hadithi_registerContent_ajax();
                        if (isLogo==1) {
                            hadithi_performAjaxPost('#submit', "Update profile information", "Updating details...",'updateprofile', '#uploadProfileForm', "/profile/");
                        } else {
                            hadithi_performAjaxPost('#submit', "Upload article", "Uploading content...",'registercontent', '#uploadContent', "/content/");
                        }
                    }
                    else if(parseInt(msg.status)==0)
                    {
                        if (isLogo==1) {
                            $('#submit').removeAttr("disabled").val("Update profile information");
                        } else {
                            $('#submit').removeAttr("disabled").val("Upload article");                                                    
                        }
                        alert (msg.txt);
                    }
                },
                error: function (data, status, e)
                    {
                        alert(e);
                        if (isLogo==1) {
                            $('#submit').removeAttr("disabled").val("Update profile information");                        
                        } else {
                            $('#submit').removeAttr("disabled").val("Upload article");                            
                        }
                    }
                }
            );
    }

    function hadithi_performAjaxOperation(postURL, finalDestination, fromVal, currID){
        $.ajax({
            type: "POST",
            url: "/"+postURL,
            data: "id="+ currID +"&valto="+fromVal,
            dataType: "json",
            success: function(msg){
                if(parseInt(msg.status)==1)
                {
                    window.location =finalDestination;
                    return true;
                }
                else if(parseInt(msg.status)==0)
                {
                    alert (msg.txt);
                }
            },
            error: function(msg){
                alert ('You seem offline. Could not contact Hadithi Service. Try again.');
            }
        });
    }
    
    function hadithi_uploadContent_ajax(){
        $('#cmdUpload').val("Uploading pdf...").attr("disabled","disabled");

            $.ajaxFileUpload(
            {
                url             : "/do_upload",
                secureuri       : false,
                fileElementId   : 'cContentFile',
                dataType        : 'json',
                success: function (msg)
                {
                    if(parseInt(msg.status)==1)
                    {
                        $("input#upl_name").val(msg.att_name);
                        $("input#upl_mime").val(msg.att_ext);
                        hadithi_registerContent_ajax();
                    }
                    else if(parseInt(msg.status)==0)
                    {
                        $('#cmdUpload').removeAttr("disabled").val("Save content");
                        alert (msg.txt);
                    }
                },
                error: function (data, status, e)
                    {
                        alert(e);
                    }
                }
            );
    }


    function hadithi_registerAccount_ajax(){
        $('#cmdUpload').val("Creating account").attr("disabled","disabled");
        $.ajax({
            type: "POST",
            url: "/createaccount",
            data: $('#uploadContent').serialize(),
            dataType: "json",
            success: function(msg){
                if(parseInt(msg.status)==1)
                {
                    window.location ="/";
                    return true;
                }
                else if(parseInt(msg.status)==0)
                {
                    $('#cmdUpload').removeAttr("disabled").val("Save content");
                    alert (msg.txt);
                }
            },
            error: function(msg){
                alert ('You seem offline. Could not contact Hadithi Service. Try again.');
                $('#cmdUpload').removeAttr("disabled").val("Save content");
            }
        });
    }
    
    
    function hadithi_registerContent_ajax(){
        $('#cmdUpload').val("Saving content...").attr("disabled","disabled");
        $.ajax({
            type: "POST",
            url: "/registercontent",
            data: $('#uploadContent').serialize(),
            dataType: "json",
            success: function(msg){
                if(parseInt(msg.status)==1)
                {
                    window.location ="/";
                    return true;
                }
                else if(parseInt(msg.status)==0)
                {
                    $('#cmdUpload').removeAttr("disabled").val("Save content");
                    alert (msg.txt);
                }
            },
            error: function(msg){
                alert ('You seem offline. Could not contact Hadithi Service. Try again.');
                $('#cmdUpload').removeAttr("disabled").val("Save content");
            }
        });
    }


    function hadithi_searchContent_ajax(){
        $('#cmdSearch').val("Searching...").attr("disabled","disabled");
        showLoader();
	$('#sub_cont').fadeIn(1500);
        $("#content #sub_cont").load("/searchcontent/" + escape($("#cSearch").val()), hideLoader());
        $('#cmdSearch').removeAttr("disabled").val("Search now");
    }


//close			
});
	

// search clearance	
function defaultInput(target){
    if((target).value == 'Find a publication'){(target).value=''}
}

function clearInput(target){
    if((target).value == ''){(target).value='Find a publication'}
}

